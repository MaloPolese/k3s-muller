# Examen K8s - PROPOSITION 1

## Applications

L'application est un petit serveur web écrit à Rust qui renvoie une page HTML statique.

Le serveur est exposé sur

- adresse: `O.O.O.O`
- port: `8000`

## CI / CD

À chaque push sur master un job de **build** et **test** est exécuté.

À chaque tag un job de **dockerization** de l'application est exécuté s'il y a eu des modifications dans le **Dockefile** ou dans le code source.

L'image docker est build et push avec **kaniko** sur le dockerhub

[> registry](https://registry.hub.docker.com/repository/docker/malopolese/polykub)

## Docker

L'image docker **rust:latest** est initialement assez importante ( 480MB ) également lorsque build de l'application plusieurs fichiers sont créés ce qui fait grossir l'image.

Si je me contente de créer mon image à parti de **rust:latest** sans rien faire d'autre mon image peut facilement faire plus 1Go.

Pour éviter vite d'en arriver là, j'ai créé mon image en deux temps:

- Build d'un exécutable de l'application à partir de **rust:latest**
- Création de l'image finale avec une **alpine:latest** dans laquelle je vais seulement copier l'exécutable créé dans l'image d'avant. Pour cela j'utilise la syntaxe:

```Dockerfile
COPY --from=<image_name>
```

Cette stratégie me permet de créer une image de seulement 7Mb.

Aussi l'image n'est pas créée en ROOT mais avec un simple utilisateur du même nom que l'application ( _polykub_ )

## Architecture

L'image ci-dessous représente l'architecture du cluster.
![alt text](./assets/archi.jpeg "Architecture")

Un **pod** est créé via un **déploiment**. Le **pod** est créé avec l'image docker de l'application crée au job précédent.
Il est lié à une **Configmap** qui contient une variable d'environement BUTTEUR.
Le **pod** est exposé sur le port 8000 et il est lié a un **service** de type **NodePort**.

Le tout est exposé sur `http:://exam.do.local` via un ingress.

`/etc/host`
![alt text](./assets/host.png "etc/host")

DEMO:
J'ai fait mon application en local donc il y a pas de demo public. 
Voici un screen de l'application en local

![alt text](./assets/app.png "demo app")