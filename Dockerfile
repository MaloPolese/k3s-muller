# select build image
FROM rust:latest as build

RUN apt-get update

RUN apt-get install musl-tools -y

RUN rustup target add x86_64-unknown-linux-musl

# create a new empty shell project
RUN USER=root cargo new --bin polykub
WORKDIR /polykub

# copy over your manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml
COPY ./Rocket.toml ./Rocket.toml
COPY ./templates ./templates

# this build step will cache your dependencies
RUN cargo build --release --target=x86_64-unknown-linux-musl
RUN rm src/*.rs

# copy your source tree
COPY ./src ./src

# build for release
RUN rm ./target/x86_64-unknown-linux-musl/release/deps/polykub*
RUN cargo build --release --target=x86_64-unknown-linux-musl

# our final base
FROM alpine:latest

RUN addgroup -g 1000 polykub

RUN adduser -D -s /bin/sh -u 1000 -G polykub polykub

# copy the build artifact from the build stage
COPY --from=build /polykub/target/x86_64-unknown-linux-musl/release/polykub .
COPY --from=build /polykub/templates ./templates
COPY --from=build /polykub/Rocket.toml ./Rocket.toml

USER polykub
# set the startup command to run your binary
CMD ["./polykub"]

