#[macro_use]
extern crate rocket;
use rocket::Request;
use rocket_dyn_templates::Template;
use std::collections::HashMap;
use std::env;

#[get("/")]
fn index() -> Template {
    let mut context = HashMap::new();
    match env::var("BUTEUR") {
        Ok(buteur) => context.insert("buteur", buteur),
        Err(_) => context.insert("buteur", String::from("ENV_BUTEUR Not found")),
    };
    // Le buteur --> Basile Boli <-- C'est lui !
    Template::render("index", &context)
}

#[catch(404)]
fn not_found(_: &Request) -> Template {
    Template::render("errors/404", &{})
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .mount("/", routes![index])
        .register("/", catchers![not_found])
        .attach(Template::fairing())
}
